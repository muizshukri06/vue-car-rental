
# Car Rental Vue 3 Application

A simple CRUD Laravel 10 & Vue 3 project



## Requirement

Requirement of this project are listed below:

- Composer
- Node js & npm
- Php 8.0 or higher
- Laravel 10
- Text editor or IDE of your choice


## Installation
Clone vue-car-rental 
```bash
    git clone https://gitlab.com/muizshukri06/vue-car-rental.git
```

Install require dependencies
```bash
    composer update
```

Install require dependencies
```bash
    npm install
```

Add .env file
```
    APP_NAME=Laravel
    APP_ENV=local
    APP_KEY=base64:5E+i002PaD1/gThdYX3cxRs2gAlpgsS/PDMgq3dfyh8=
    APP_DEBUG=true
    APP_URL=http://localhost

    LOG_CHANNEL=stack
    LOG_DEPRECATIONS_CHANNEL=null
    LOG_LEVEL=debug

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=carrent
    DB_USERNAME=root
    DB_PASSWORD=

    BROADCAST_DRIVER=log
    CACHE_DRIVER=file
    FILESYSTEM_DISK=local
    QUEUE_CONNECTION=sync
    SESSION_DRIVER=file
    SESSION_LIFETIME=120

    MEMCACHED_HOST=127.0.0.1

    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379

    MAIL_MAILER=smtp
    MAIL_HOST=mailpit
    MAIL_PORT=1025
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    MAIL_FROM_ADDRESS="hello@example.com"
    MAIL_FROM_NAME="${APP_NAME}"

    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_DEFAULT_REGION=us-east-1
    AWS_BUCKET=
    AWS_USE_PATH_STYLE_ENDPOINT=false

    PUSHER_APP_ID=
    PUSHER_APP_KEY=
    PUSHER_APP_SECRET=
    PUSHER_HOST=
    PUSHER_PORT=443
    PUSHER_SCHEME=https
    PUSHER_APP_CLUSTER=mt1

    VITE_APP_NAME="${APP_NAME}"
    VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    VITE_PUSHER_HOST="${PUSHER_HOST}"
    VITE_PUSHER_PORT="${PUSHER_PORT}"
    VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
    VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```

Generate application key
```bash
    php artisan key:generate
```
## Run Application

To run application, run the following command
```bash
  php artisan serve
```

Then run the following command
```bash
  npm run watch
```

make sure to keep ```php artisan serve``` and  ```npm run watch``` always running while using the application.


