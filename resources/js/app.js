import './bootstrap';
import { createApp } from 'vue';
import { createRouter, createWebHistory } from "vue-router";

import App from './components/App.vue';
import RentList from './components/RentList.vue';
import RentForm from './components/RentForm.vue';
// import Example from './components/ExampleComponent.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: "/", component: RentList },
        { path: "/rent/create", component: RentForm },
    ]
});

const app = createApp(App);
app.use(router);
app.mount('#app');

// const app2 = createApp(Example);
// app2.mount('#app2');
